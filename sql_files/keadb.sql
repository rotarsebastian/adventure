create database keadb;

use keadb;

create table CUSTOMERS(
CUS_ID int primary key auto_increment,
NAME varchar(50),
AGE int,
CUS_TYPE varchar(50));

create table ACTIVITIES(
ACT_ID int primary key auto_increment,
NAME varchar(50),
PRICE double);

create table RESERVED_ACT(
RES_ID int primary key auto_increment,
CUS_ID int,
ACT_ID int);

create table RESERVATIONS(
ID int primary key auto_increment,
RES_ID int,
PAYMENT double);

